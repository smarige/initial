/* Generated automatically. */
static const char configuration_arguments[] = "/home/sathya/riscv-tools/riscv-gnu-toolchain/build/../riscv-gcc/configure --target=riscv64-unknown-elf --prefix=/media/sathya/secondary_drive/tools --disable-shared --disable-threads --enable-languages=c,c++ --with-system-zlib --enable-tls --with-newlib --with-sysroot=/media/sathya/secondary_drive/tools/riscv64-unknown-elf --with-native-system-header-dir=/include --disable-libmudflap --disable-libssp --disable-libquadmath --disable-libgomp --disable-nls --src=../../riscv-gcc --enable-checking=yes --disable-multilib --with-abi=lp64d --with-arch=rv64imafdc 'CFLAGS_FOR_TARGET=-Os  -mcmodel=medany'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "lp64d" }, { "arch", "rv64imafdc" } };
